# AWS + Ansible + NVE

Let me explain what this is all about in a nutshell. This Ansible Playbook helps in importing NVE to AWS S3 bucket and converts the VMDK to AMI. From there you can spin up "n" number of instances as per your requirement.

# NVE - NetWorker Virtual edition

NetWorker ® Virtual Edition (NVE) is a NetWorker Server that runs as a virtual machine in a VMware environment. NVE integrates the latest version of the NetWorker software with SuSE Linux as a VMware virtual machine. NVE is available as a 250 GB virtual appliance.

# What does this Playbook do for you?
* Creates a S3 Bucket to hold the VMDK file
* Uploads the NVE VMDK to the S3 Bucket
* Creates an IAM role
* Attaches a Policy to the created role
* Imports the VMDK from S3 and converts it into an Amazon Machine Image (AMI)

# Limitation
 There are few modules that Ansible lacks inorder to perform certain tasks. However the Ansible community is rapidly working on it to fullfil that gap. This Playbook lacks the Ansible module to perform a VM import-image. Hence I have substituted it by the **`command`** module which calls **`aws cli`** in the background to perform them. 

---

# Screenshots of the playbook output and the resources created on AWS

![](https://3.bp.blogspot.com/-Mlw4iVphxhg/W1eUEWuXnjI/AAAAAAAAAfs/HY6Fwu9sT0AVZ9URUg65rSyeTiVOhK9iACLcBGAs/s1600/PLAYBOOK.png)
![](https://3.bp.blogspot.com/-QVjRhv08XWg/W1eUFaUZNGI/AAAAAAAAAf0/TfMzbfTL1JUMgXlRQ1v_8uQlJBKrqTrgwCLcBGAs/s1600/S3_Bucket.png)
![](https://2.bp.blogspot.com/-9bhZxp7nyI0/W1eUFJzxhcI/AAAAAAAAAfw/qEdt_i1fafsw0qHxW1ePsmgho3fgQ6XawCLcBGAs/s1600/ROLE_POLICY.png)
![](https://1.bp.blogspot.com/--PlWR03HyYQ/W1eUEMY_T2I/AAAAAAAAAfo/wYlwEvt9ahcr7dIO7KkYRSHGuApbAn3DACLcBGAs/s1600/DESCRIBE_IMPORT.png)
![](https://2.bp.blogspot.com/-hXj1O_Wm6f4/W1eUFo0BFHI/AAAAAAAAAf4/c2oYSIFo_RINVcWFB2-xSPAx9xYC6tabQCLcBGAs/s1600/SNAPSHOT.png)
![](https://3.bp.blogspot.com/-CHGYFERGGE8/W1ec1I0Xr5I/AAAAAAAAAg0/DC1DLK4JfY8yPxMYQs-fKd8H417S2vOWwCLcBGAs/s1600/AMI.png)


---
# Contributors

- Stalin Stepin <stalin.stepin@outlook.com>

---
# License & Copyright

© Stalin Stepin

Licensed under the [MIT License](LICENSE).
 



